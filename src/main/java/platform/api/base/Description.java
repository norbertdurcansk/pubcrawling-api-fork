package platform.api.base;

/**
 * *
 * Description for Api method
 * Created by Norbert on 4/10/2016.
 */
public class Description {

    public  enum ApiCodes{OK,NOT_AVAILABLE,INVALID_ACCESS,BAD_REQUEST,INTERNAL_SERVER_ERROR,CONFLICT}
    public enum  ApiObjects{JSON}

    private String path;
    private String version;
    private String description;
    private String auth;
    private String produces;
    private ApiCodes responseCode;
    private ApiCodes error;
    private String consumes;
    private String responseObject;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public String getProduces() {
        return produces;
    }

    public void setProduces(String produces) {
        this.produces = produces;
    }

    public ApiCodes getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(ApiCodes responseCode) {
        this.responseCode = responseCode;
    }

    public ApiCodes getError() {
        return error;
    }

    public void setError(ApiCodes error) {
        this.error = error;
    }

    public String getConsumes() {
        return consumes;
    }

    public void setConsumes(String consumes) {
        this.consumes = consumes;
    }

    public String getResponseObject() {
        return responseObject;
    }

    public void setResponseObject(String responseObject) {
        this.responseObject = responseObject;
    }
}
