package platform.api.base.anotation;

/**
 * Created by Norbert on 4/13/2016.
 */
public @interface Api {

    String name() default "";
    String description() default  "";
}
