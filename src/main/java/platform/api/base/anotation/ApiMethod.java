package platform.api.base.anotation;

import platform.api.base.Description;

/**
 * Created by Norbert on 4/13/2016.
 */
public @interface ApiMethod {
    String name();
    String description();
    Description.ApiCodes responsestatuscode();
    Description.ApiCodes[] error();

}
