package platform.api.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;



@Controller
public class HomeController {

    @RequestMapping("/")
    public ModelAndView initPage() {

      /*  DSLContext dsl = dslFactoryService.dslContextGlobal();
        Long first = dsl.select(APP.ID).from(APP).where(APP.ID.eq(1)).fetchOne(0,Long.class);*/

        return new ModelAndView("index", "message", "first page");
    }
}