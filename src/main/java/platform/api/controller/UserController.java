package platform.api.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import platform.api.base.Description;
import platform.api.base.anotation.Api;
import platform.api.base.anotation.ApiMethod;
import platform.api.impl.User;
import platform.api.services.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Norbert on 4/13/2016.
 */


@Controller
@RequestMapping(value = "/users")
@Api(name = "User Service", description = "Overall user handling service")
public class UserController {

    @Autowired
    UserService userService;

    @ApiMethod(name = "Create User", description = "Returns created user object", responsestatuscode = Description.ApiCodes.OK,
    error ={Description.ApiCodes.BAD_REQUEST, Description.ApiCodes.INTERNAL_SERVER_ERROR, Description.ApiCodes.CONFLICT} )
    @RequestMapping(value = { "/create" }, method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
    public @ResponseBody User createUser( @RequestBody  User user) throws Exception {
        user=userService.finalize(user);
        user=userService.create(user);
        return  user;
    }


}
