package platform.api.services;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.springframework.beans.propertyeditors.StringArrayPropertyEditor;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.DriverManager;


/**
 * Created by noro on 8.4.2016.
 */

@Service
public class DSLContextFactoryService {

    private static final String userName="root";
    private static final String password="root";
    private static final String url = "jdbc:mysql://localhost:3306/app";

    public  DSLContext dslContextGlobal(){
        /** create context */
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            Connection conn = DriverManager.getConnection(url, userName, password);
            return DSL.using(conn, SQLDialect.MYSQL);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
