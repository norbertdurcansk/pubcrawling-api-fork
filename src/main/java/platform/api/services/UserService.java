package platform.api.services;

import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.export.naming.IdentityNamingStrategy;
import org.springframework.stereotype.Service;
import platform.api.base.exceptions.BadRequestException;
import platform.api.base.exceptions.EmailExistsException;
import platform.api.base.exceptions.InternalServerErrorException;
import platform.api.impl.User;
import platform.api.impl.generated.tables.records.UserRecord;
import platform.api.services.interfaces.DatabaseOperations;
import  static  platform.api.impl.generated.tables.User.USER;


import java.sql.Timestamp;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Created by Norbert on 4/13/2016.
 */
@Service
public class UserService implements DatabaseOperations<User> {
    private final static Logger log = Logger.getLogger(UserService.class.getName());

    @Autowired
    DSLContextFactoryService dslContextFactoryService;

    public User finalize(User object) throws Exception {
        if(!object.getToken().equals("") || object.getPassword().equals("") || object.getEmail().equals("")
                || object.getFirstname().equals("") || object.getSurname().equals("")) {

           throw new BadRequestException();
        }
        /** check  if email exists */
        if(getCountEmail(object.getEmail())!=0) {
            throw new EmailExistsException();
        }
        Date today = new Date();
        object.setCreated(new Timestamp(today.getTime()));
        /** generate unique token */
        object.setToken(tokenGenerator());
        return object;
    }

    private Integer getCountEmail(String email)
    {
        DSLContext dao = dslContextFactoryService.dslContextGlobal();
        return dao.selectCount().from(USER).where(USER.EMAIL.eq(email)).fetchOne(0, Integer.class);
    }

    private String tokenGenerator() throws Exception {
        char[] CHARS = "AaBbCcDdEeFfHhKkMmNnPpQqRrSsTtVvWwXxYyZ".toCharArray();
        StringBuilder sb = new StringBuilder();
        Integer count = -1;
        while (count != 0) {
            Random random = new Random();
            for (int i = 0; i <11; i++) {
                char c = CHARS[random.nextInt(CHARS.length)];
                sb.append(c);
            }
            count = getCountToken(sb.toString());
        }
        return sb.toString();
    }

    private Integer getCountToken(String globalFilter) throws Exception {
        DSLContext dao = dslContextFactoryService.dslContextGlobal();
        return dao.selectCount().from(USER).where(USER.TOKEN.eq(globalFilter)).fetchOne(0, Integer.class);
    }

    public User update(User object) throws Exception {
        return null;
    }

    public User create(User object) throws Exception {
        try {
        DSLContext dsl = dslContextFactoryService.dslContextGlobal();
        UserRecord userRecord = dsl.newRecord(USER,object);
        userRecord.store();
        }catch (Exception e )
        {
            log.log(Level.SEVERE,"Error creating user");
            throw new InternalServerErrorException();
        }
        log.log(Level.INFO,"User Created - {0}",object.getToken());
        return object;
    }

    public User delete(User object) throws Exception {
        return null;
    }

    public User findOne(Long id) throws Exception {
        return null;
    }
}
