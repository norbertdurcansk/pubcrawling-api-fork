package platform.api.services.interfaces;

/**
 * Created by Norbert on 4/13/2016.
 */
public interface DatabaseOperations<A> {

    public  A finalize(A object) throws Exception;
    public  A update(A object) throws Exception;
    public A create(A object)  throws Exception;
    public A delete(A object)  throws Exception;
    public A findOne(Long id) throws  Exception;

}
