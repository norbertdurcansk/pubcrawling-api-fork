<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<spring:url value="/resources/plugins/semantic-ui/semantic.min.css" var="semanticCSS"/>
<spring:url value="/resources/plugins/semantic-ui/semantic.min.js" var="semanticJS"/>
<spring:url value="/resources/js/init/init.js" var="init"/>
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<link href="${semanticCSS}" rel="stylesheet"/>
<script src="${semanticJS}"></script>
<script src="${init}"></script>

<html>
<head>
    <title>Api</title>
</head>
<body>
<h1>API PubCrawling</h1>

<form action="/users/create" method="POST" enctype="application/json">
    First name:<br>
    <input type="text" name="firstname" value="Mickey"><br>
    Last name:<br>
    <input type="text" name="surname" value="Mouse"><br><br>
    <input type="submit" value="Submit">
</form>


</body>


</html>